import React from 'react';

import { MyAppComponent } from './components';

const App: React.FC = () => {
  return (
    <div className="App">
      <h1 className="App-header">
        React dependencies and hooks example
      </h1>

      <MyAppComponent bar='bar'/>
    </div>
  );
}

export default App;
