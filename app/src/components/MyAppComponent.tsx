import React from 'react';

interface MyAppComponentProps {
    bar: string;
}

export const MyAppComponent: React.FC<MyAppComponentProps> = (props) => {
    return (
        <div>I'm MyAppComponent with bar property = {props.bar}</div>
    );
};
