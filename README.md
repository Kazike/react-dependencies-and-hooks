# react-dependencies-and-hooks

This is a sample project to illustrate the problem of having a library project with React dependencies, included in another app project with also React dependencies.

The invalid hook call warning will appear if you use hooks in the library project, most possibly because this configuration will create two instances of React running at the same time.