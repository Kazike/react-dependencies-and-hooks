import React from 'react';

interface MyLibComponentProps {
    foo: string;
}

export const MyLibComponent: React.FC<MyLibComponentProps> = (props) => {
    return (
        <div>I'm MyLibComponent with foo property = {props.foo}</div>
    );
};
